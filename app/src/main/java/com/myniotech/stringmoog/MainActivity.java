package com.myniotech.stringmoog;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.myniotech.stringmoog.dao.PresetDao;

import org.puredata.android.io.AudioParameters;
import org.puredata.android.service.PdService;
import org.puredata.core.PdBase;
import org.puredata.core.utils.IoUtils;

import java.io.File;
import java.io.IOException;

import io.fabric.sdk.android.Fabric;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;


@RuntimePermissions
public class MainActivity extends AppCompatActivity {

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE})
    void requestPermissions() {
        //exibe dialog solicitando permissões
    }


    @OnShowRationale({Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE})
    void showRationaleForGetPosition(PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permissions)
                .setPositiveButton(R.string.button_allow, (dialog, button) -> request.proceed())
                .setNegativeButton(R.string.button_deny, (dialog, button) -> request.cancel())
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    private PdService pdService = null;

    private final ServiceConnection pdConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            pdService = ((PdService.PdBinder) service).getService(); //1
            try {
                initPd(); //2
                loadPatch0();//3

            } catch (IOException e) {
                finish();//4
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            //esse método nunca será chamado
        }
    };

    private void start() {
        if (!pdService.isRunning()) { //6
            Intent intent = new Intent(MainActivity.this, MainActivity.class); //7
            pdService.startAudio(intent, R.drawable.ic_action_piano,
                    "StringMoog", "Return to StringMoog"); //8
        }
    }

    private void initPd() throws IOException {//1

        AudioParameters.init(this);//2
        int sampleRate = AudioParameters.suggestSampleRate();//3
        pdService.initAudio(sampleRate, 0, 2, 10.0f);//4
        start();//5

    }

    private void loadPatch0() throws IOException {//1
        File dir = getFilesDir();//2
        IoUtils.extractZipResource(
                getResources().openRawResource(R.raw.stringmoog), dir, true);//3
        File patchFile = new File(dir, "stringmoog.pd");//4
        PdBase.openPatch(patchFile.getAbsolutePath());//5
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindService(pdConnection);
    }

    // Interrompe o processo de áudio caso for recebida uma chamada telefônica
    private void initSystemServices() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                if (pdService == null)
                    return;
                if (state == TelephonyManager.CALL_STATE_IDLE) {
                    start();
                } else {
                    pdService.stopAudio();
                }
            }
        }, PhoneStateListener.LISTEN_CALL_STATE);
    }

    public void replaceFragment(Fragment fragment) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.main_fragment, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fabric.with(this, new Crashlytics());
        initSystemServices();

        // cria automaticamente o service assim que a ligação (binding) existir.
        bindService(new Intent(this, PdService.class), pdConnection,
                BIND_AUTO_CREATE);

        MainActivityPermissionsDispatcher.requestPermissionsWithCheck(this);



    }

    @Override
    protected void onStart() {
        super.onStart();
        if (PresetDao.getInstance().getPresets().isEmpty()) {
            PresetDao.getInstance().loadBasicList();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        replaceFragment(LauncherFragment.newInstance());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.adsr:
                replaceFragment(ADSRFragment.newInstance());
                return true;
            case R.id.piano:
                replaceFragment(PianoFragment.newInstance(60, true));
                return true;
            case R.id.home:
                replaceFragment(LauncherFragment.newInstance());
                return true;
            case R.id.crash:
                throw new RuntimeException("This is a crash");
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
