package com.myniotech.stringmoog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.myniotech.stringmoog.adapter.PresetAdapter;
import com.myniotech.stringmoog.dao.PresetDao;
import com.myniotech.stringmoog.model.ADSRPreset;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.puredata.core.PdBase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by luiz on 05/09/16.
 */
public class ADSRFragment extends Fragment {

    @BindView(R.id.presetList)
    ListView presetList;
    @BindView(R.id.edtAttack)
    DiscreteSeekBar edtAttack;
    @BindView(R.id.edtDecay)
    DiscreteSeekBar edtDecay;
    @BindView(R.id.edtSustain)
    DiscreteSeekBar edtSustain;
    @BindView(R.id.edtRelease)
    DiscreteSeekBar edtRelease;
    @BindView(R.id.btnSave)
    Button btnSave;

    @BindView(R.id.presetSavedName)
    TextView presetSavedName;

    @OnClick(R.id.btnPlay)
    public void playPreset() {
        setValues();
        ((MainActivity) getActivity()).replaceFragment(PianoFragment.newInstance(60, false));
    }

    public static ADSRFragment newInstance() {
        Bundle args = new Bundle();
        ADSRFragment fragment = new ADSRFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        this.setRetainInstance(true);
        View rootView = inflater.inflate(R.layout.adsr_fragment, container, false);

        ButterKnife.bind(this, rootView);

        btnSave.setOnClickListener(view -> openSavePresetFragment());

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshList();
    }

    public void setValues() {

        PdBase.sendFloat("attack", edtAttack.getProgress());
        PdBase.sendFloat("decay", edtDecay.getProgress());
        PdBase.sendFloat("sustain", edtSustain.getProgress());
        PdBase.sendFloat("release", edtRelease.getProgress());

    }

    public void setValues(ADSRPreset adsrPreset) {

        edtAttack.setProgress((int) adsrPreset.getAttack());
        edtDecay.setProgress((int) adsrPreset.getDecay());
        edtSustain.setProgress((int) adsrPreset.getSustain());
        edtRelease.setProgress((int) adsrPreset.getRelease());
        presetSavedName.setText(adsrPreset.getName());

        PdBase.sendFloat("attack", adsrPreset.getAttack());
        PdBase.sendFloat("decay", adsrPreset.getDecay());
        PdBase.sendFloat("sustain", adsrPreset.getSustain());
        PdBase.sendFloat("release", adsrPreset.getRelease());
    }

    public ADSRPreset savePreset() {

        float attack = edtAttack.getProgress();
        float decay = edtDecay.getProgress();
        float sustain = edtSustain.getProgress();
        float release = edtRelease.getProgress();

        return new ADSRPreset(attack, decay, sustain, release, "");

    }

    private void openSavePresetFragment() {
        ((MainActivity) getActivity()).replaceFragment(SavePresetFragment.newInstance(savePreset()));
    }

    public void getLastPreset() {

        ADSRPreset result = PresetDao.getInstance().getLastPreset();
        edtAttack.setProgress((int) result.getAttack());
        edtDecay.setProgress((int) result.getDecay());
        edtSustain.setProgress((int) result.getSustain());
        edtRelease.setProgress((int) result.getRelease());
        presetSavedName.setText(result.getName());

    }

    public void refreshList() {

        PresetAdapter presetAdapter = new PresetAdapter(this.getContext(), PresetDao.getInstance().getPresets());
        presetList.setAdapter(presetAdapter);
        presetList.invalidate();

        presetList.setOnItemClickListener((adapterView, view, i, l)
                -> setValues((ADSRPreset) presetAdapter.getItem(i)));

        presetList.setOnItemLongClickListener((adapterView, view, i, l)
                -> {
            PresetDao.getInstance().deletePreset(i);
            refreshList();
            return true;
        });

        try {
            getLastPreset();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
