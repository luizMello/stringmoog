package com.myniotech.stringmoog.dao;

import android.widget.Toast;

import com.myniotech.stringmoog.ADSRFragment;
import com.myniotech.stringmoog.MainActivity;
import com.myniotech.stringmoog.model.ADSRPreset;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by luiz on 14/09/16.
 */
public class PresetDao {
    Realm realm;

    public PresetDao() {
        realm = Realm.getDefaultInstance();
    }

    public static PresetDao getInstance() {
        return new PresetDao();
    }

    public void deletePreset(int i) {
        // obtain the results of a query
        final RealmResults<ADSRPreset> results = realm.where(ADSRPreset.class).findAll();

        // All changes to data must happen in a transaction
        realm.executeTransaction(realm1 -> {
            // remove a single object
            ADSRPreset adsrPreset = results.get(i);
            adsrPreset.deleteFromRealm();
        });
    }

    public boolean savePreset(ADSRPreset adsrPreset, String presetName) {

        try {
            realm.beginTransaction();
            realm.copyToRealm(new ADSRPreset(
                    adsrPreset.getAttack(),
                    adsrPreset.getDecay(),
                    adsrPreset.getSustain(),
                    adsrPreset.getRelease(), presetName));
            realm.commitTransaction();
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public boolean savePreset(ADSRPreset adsrPreset) {

        try {
            realm.beginTransaction();
            realm.copyToRealm(adsrPreset);
            realm.commitTransaction();
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public ADSRPreset getLastPreset() {
        try {
            RealmQuery<ADSRPreset> query = realm.where(ADSRPreset.class);
            return query.findAll().last();
        } catch (Exception e) {
            return null;
        }
    }

    public ArrayList<ADSRPreset> getPresets() {
        RealmQuery<ADSRPreset> query = realm.where(ADSRPreset.class);
        RealmResults<ADSRPreset> result1 = query.findAll();
        ArrayList<ADSRPreset> adsrPresets = new ArrayList<>();

        for (ADSRPreset adsr : result1) {
            adsrPresets.add(adsr);
        }

        return adsrPresets;
    }

    public void loadBasicList(){
        savePreset(new ADSRPreset(10, 1600, 0, 0, "Preset 1"));
        savePreset(new ADSRPreset(50, 432, 1, 2, "Preset 2"));
        savePreset(new ADSRPreset(20, 323, 0, 0, "Preset 3"));
        savePreset(new ADSRPreset(80, 1000, 2, 0, "Preset 4"));
    }
}
