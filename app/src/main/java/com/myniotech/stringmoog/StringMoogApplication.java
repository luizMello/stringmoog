package com.myniotech.stringmoog;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by luiz on 14/08/16.
 */
public class StringMoogApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }

}
