package com.myniotech.stringmoog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.myniotech.stringmoog.dao.PresetDao;
import com.myniotech.stringmoog.model.ADSRPreset;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SavePresetFragment extends Fragment {

    @BindView(R.id.savePresetName)
    EditText savePresetName;

    static ADSRPreset mAdsrPreset;

    @OnClick(R.id.btnSavePreset)
    public void btnSavePreset() {

        if (PresetDao.getInstance().savePreset(mAdsrPreset,String.valueOf(savePresetName.getText()))) {
            Toast.makeText(
                    getActivity(), "Preset Saved", Toast.LENGTH_SHORT).show();
            ((MainActivity) getActivity()).replaceFragment(ADSRFragment.newInstance());
        } else {
            Toast.makeText(
                    getActivity(), "Error on save preset", Toast.LENGTH_SHORT).show();
        }

    }

    public SavePresetFragment() {
    }

    public static SavePresetFragment newInstance(ADSRPreset adsrPreset) {
        mAdsrPreset = adsrPreset;
        SavePresetFragment frag = new SavePresetFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.setRetainInstance(true);

        return inflater.inflate(R.layout.fragment_edit_name, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        savePresetName.requestFocus();

    }

}