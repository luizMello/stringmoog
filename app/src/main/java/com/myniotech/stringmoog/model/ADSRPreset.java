package com.myniotech.stringmoog.model;

import io.realm.RealmObject;

/**
 * Created by luiz on 07/09/16.
 */
public class ADSRPreset extends RealmObject {

    float attack;
    float decay;
    float sustain;
    float release;
    String name;

    public ADSRPreset(float attack, float decay, float sustain, float release, String name) {
        this.attack = attack;
        this.decay = decay;
        this.sustain = sustain;
        this.release = release;
        this.name = name;
    }

    public ADSRPreset() {
    }

    public float getAttack() {
        return attack;
    }

    public void setAttack(float attack) {
        this.attack = attack;
    }

    public float getDecay() {
        return decay;
    }

    public void setDecay(float decay) {
        this.decay = decay;
    }

    public float getSustain() {
        return sustain;
    }

    public void setSustain(float sustain) {
        this.sustain = sustain;
    }

    public float getRelease() {
        return release;
    }

    public void setRelease(float release) {
        this.release = release;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

