package com.myniotech.stringmoog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.myniotech.stringmoog.dao.PresetDao;
import com.myniotech.stringmoog.model.ADSRPreset;
import com.myniotech.stringmoog.pianoview.Piano;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.puredata.core.PdBase;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by luiz on 05/09/16.
 */
public class PianoFragment extends Fragment {

    private int mRootNote;
    Piano piano;

    @BindView(R.id.modulation)
    DiscreteSeekBar seekBarModulation;

    @BindView(R.id.idx_mod)
    DiscreteSeekBar seekBaridxMod;

    public static PianoFragment newInstance(int rootNote, boolean loadLast) {
        Bundle args = new Bundle();
        args.putInt("rootNote", rootNote);
        args.putBoolean("loadLast", loadLast);
        PianoFragment fragment = new PianoFragment();
        fragment.setArguments(args);
        return fragment;
    }


    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            mRootNote = bundle.getInt("rootNote");
            boolean loadLast = bundle.getBoolean("loadLast");
            if (loadLast) {
                ADSRPreset adsrPreset = PresetDao.getInstance().getLastPreset();
                if (adsrPreset != null) {
                    setValues(adsrPreset);
                }
            }
        }
    }

    public void setValues(ADSRPreset adsrPreset) {
        PdBase.sendFloat("attack", adsrPreset.getAttack());
        PdBase.sendFloat("decay", adsrPreset.getDecay());
        PdBase.sendFloat("sustain", adsrPreset.getSustain());
        PdBase.sendFloat("release", adsrPreset.getRelease());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        View rootView = inflater.inflate(R.layout.piano_fragment, container, false);

        piano = (Piano) rootView.findViewById(R.id.pianoView);
        piano.setPianoKeyListener(onPianoKeyPress);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        readBundle(getArguments());

        seekBarModulation.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                PdBase.sendFloat("modulation", seekBarModulation.getProgress());
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

            }
        });

        seekBaridxMod.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                PdBase.sendFloat("idx", seekBaridxMod.getProgress());
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

            }
        });

    }

    private Piano.PianoKeyListener onPianoKeyPress =
            (id, action) -> {

                if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN) {

                    if (id == 0) {
                        PdBase.sendFloat("note1", mRootNote);
                    }
                    if (id == 1) {
                        PdBase.sendFloat("note1", mRootNote + 1);
                    }
                    if (id == 2) {
                        PdBase.sendFloat("note1", mRootNote + 2);
                    }
                    if (id == 3) {
                        PdBase.sendFloat("note1", mRootNote + 3);
                    }
                    if (id == 4) {
                        PdBase.sendFloat("note1", mRootNote + 4);
                    }
                    if (id == 5) {
                        PdBase.sendFloat("note1", mRootNote + 5);
                    }
                    if (id == 6) {
                        PdBase.sendFloat("note1", mRootNote + 6);
                    }
                    if (id == 7) {
                        PdBase.sendFloat("note1", mRootNote + 7);
                    }
                    if (id == 8) {
                        PdBase.sendFloat("note1", mRootNote + 8);
                    }
                    if (id == 9) {
                        PdBase.sendFloat("note1", mRootNote + 9);
                    }
                    if (id == 10) {
                        PdBase.sendFloat("note1", mRootNote + 10);
                    }
                    if (id == 11) {
                        PdBase.sendFloat("note1", mRootNote + 11);
                    }
                    if (id == 12) {
                        PdBase.sendFloat("note1", mRootNote + 12);
                    }
                }

            };
}
