package com.myniotech.stringmoog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by luiz on 05/09/16.
 */
public class LauncherFragment extends Fragment {

    @OnClick(R.id.btnOpenPiano)
    public void openPiano(){
        ((MainActivity) getActivity()).replaceFragment(PianoFragment.newInstance(60,true));
    }

    @OnClick(R.id.btnOpenADSR)
    public void openAdsr(){
        ((MainActivity) getActivity()).replaceFragment(ADSRFragment.newInstance());
    }

    public static LauncherFragment newInstance() {
        return new LauncherFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        return inflater.inflate(R.layout.launcher_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

}
