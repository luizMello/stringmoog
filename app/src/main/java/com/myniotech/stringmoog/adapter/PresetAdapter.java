package com.myniotech.stringmoog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.myniotech.stringmoog.R;
import com.myniotech.stringmoog.model.ADSRPreset;

import java.util.ArrayList;

public class PresetAdapter extends BaseAdapter implements ListAdapter {
    private final Context context;
    private final ArrayList<ADSRPreset> values;

    public PresetAdapter(Context context, ArrayList<ADSRPreset> values) {
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int i) {
        return values.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.preset_item, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.presetName);
        TextView a = (TextView) rowView.findViewById(R.id.attack);
        TextView d = (TextView) rowView.findViewById(R.id.decay);
        TextView s = (TextView) rowView.findViewById(R.id.sustain);
        TextView r = (TextView) rowView.findViewById(R.id.release);

        textView.setText(values.get(position).getName());
        a.setText("A: " + String.valueOf((int) values.get(position).getAttack()));
        d.setText("D: " + String.valueOf((int) values.get(position).getDecay()));
        s.setText("S: " + String.valueOf((int) values.get(position).getSustain()));
        r.setText("R: " + String.valueOf((int) values.get(position).getRelease()));

        return rowView;
    }

}